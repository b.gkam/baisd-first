def change_coin(cointype, amount):
    change = []

    for coin in cointype:

        while amount:
            if coin <= amount:
                change.append(coin)
                amount -= coin
            else:
                break
    return change


if __name__ == "__main__":
    coin_input = int(input("Enter your coin : "))

    cointype = [1, 2, 4, 5, 10]
    cointype.sort(reverse=True)

    if cointype[-1] != 1:
        cointype.append(1)

    amount = coin_input
    print(change_coin(cointype, amount))
