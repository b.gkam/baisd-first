def change_coin(change, coins):
    change_start = dict()

    for c in reversed(sorted(coins)):
        change_start[c] = change // c
        change = change % c

    return change_start


if __name__ == "__main__":
    change = int(input("Enter your coin : "))
    coins = [1, 2, 4, 5, 10]

    print(change_coin(change, coins))
