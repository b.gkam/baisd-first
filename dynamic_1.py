import timeit


def dnm(n):
    f0 = 0
    f1 = 1
    f2 = 1
    for i in range(n):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    n = int(input("Enter number: "))
    ans = dnm(n)
    print(ans)


# n = input("Input number of Fibonacci number: ")
# n = int(n)

# def fib(n):
#     lst = [0, 1]
#     ans = ""
#     val = []

#     for i in range(2, n + 1):
#         val = lst[i - 1] + lst[i - 2]
#         lst.append(val)

#     for x in lst:
#         ans += str(x)
#         ans += ", "
#     return ans[0 : len(ans) - 2]


# print(fib(n))
